package org.crossref.refmatching;

import io.sentry.Sentry;
import io.sentry.event.Event;
import io.sentry.event.EventBuilder;
import io.sentry.event.Sdk;
import io.sentry.event.interfaces.ExceptionInterface;
import java.util.Date;
import static org.apache.commons.lang3.StringUtils.isEmpty;
import org.apache.log4j.*;

public class SentryLogger extends Logger {

    private final org.apache.log4j.Logger logger;

    // Flag to turn event capturing on/off
    private boolean captureEvents = true;
    private String dsnValue = "";

    public SentryLogger(Logger logger) {
        super(logger.getName());
        this.logger = logger;
    }

    public static SentryLogger getLogger(String name) {
        return new SentryLogger(org.apache.log4j.Logger.getLogger(name));
    }

    public static SentryLogger getLoggerClass(Class<?> clazz) {
        return getLogger(clazz.getName());
    }

    public void setCaptureEvents(boolean captureEvents) {
        this.captureEvents = captureEvents;
    }
    
    public void setDSN(String dsn) {
        this.dsnValue = dsn;
        Sentry.init(dsnValue);
    }

    @Override
    public void error(Object message, Throwable t) {
        logger.error(message, t);
        recordException(t, message.toString());
    }

    public void error(Throwable cause) {
        String fm = cause.getLocalizedMessage();
        logger.error(fm, cause);
        recordException(cause, fm);

    }

    /**
     * Record a given exception
     *
     * @param t The exception to record
     * @param message Message associated with exception
     * @return True if the exception was submitted to Sentry, false if not (i.e.
     * disabled/filtered out)
     */
    public boolean recordException(Throwable t, String message) {

        // Only submit when capturing events
        if (!captureEvents || dsnValue.isEmpty()) {
            return false;
        }
        

        // Using the Throwable description as a work around as it contains the missing parameter values. 
        // This is because the ExceptionInterface uses the detailMessage in the throwable and ignores the parameters list
        // This originally resulting in "identity={0}" instead of "identity=wjst" 
        // Note: the ExceptionInterface is legacy code and is removed in the newer sentry versions. 
        Throwable p = new Throwable(t.toString(), t);

        // Initialize new event
        EventBuilder eventBuilder = new EventBuilder()
                .withSentryInterface(new ExceptionInterface(p))
                .withMessage(message)
                .withTimestamp(new Date());

        // Initialize event finger print to be: {exceptionclass}
        StringBuilder fingerprint = new StringBuilder(t.getClass().getName());

        // If possible, record error method and extend fingerprint with it
        StackTraceElement[] stElems = t.getStackTrace();
        if (stElems != null && stElems.length > 0) {
            StackTraceElement stElem = stElems[0];
            if (!isEmpty(stElem.getClassName()) && !isEmpty(stElem.getMethodName())) {
                String errorMethod = stElem.getClassName() + '.' + stElem.getMethodName();
                eventBuilder.withTag("error_method", errorMethod);
                fingerprint.append(":").append(errorMethod);
            }
        }

        eventBuilder.withFingerprint(fingerprint.toString());
        Event e = eventBuilder.getEvent();

        // This is a hack to get around a Sentry bug - without it
        // Sentry throws an NPE internally and quietly. The docs
        // don't describe this object, so these values are somewhat
        // arbitrary, but they work and are reflective of Sentry info
        e.setSdk(new Sdk("java", "1.7", null));

        // This sends an exception event to Sentry.
        Sentry.capture(e);
        return true;
    }
}
