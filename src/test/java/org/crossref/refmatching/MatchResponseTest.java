package org.crossref.refmatching;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import org.junit.Test;
import org.junit.Assert;

/**
 *
 * @author Dominika Tkaczyk
 */
public class MatchResponseTest {

    @Test
    public void testMatchResponse() {
        MatchRequest request = new MatchRequest(
            IntStream.range(0, 10)
                    .mapToObj(i -> new ReferenceData(new Reference("ref " + i)))
                    .collect(Collectors.toList())
        );
        List<ReferenceLink> links = IntStream.range(0, 10)
                .mapToObj(i -> new ReferenceLink(
                        new ReferenceData(new Reference("ref " + i)),
                        "doi " + i, i))
                .collect(Collectors.toList());
        
        MatchResponse response = new MatchResponse(request, links);
        
        Assert.assertEquals(10, response.getMatchedLinks().size());
        for (int i = 0; i < 10; i++) {
            Assert.assertEquals(i, response.getMatchedLinks().get(i).getScore(),
                    0.0001);
            Assert.assertEquals("doi " + i,
                    response.getMatchedLinks().get(i).getDOI());
            Assert.assertEquals("ref " + i,
                    response.getMatchedLinks().get(i).getReferenceData()
                            .getReference().getFormattedString());
        }
        
        Assert.assertEquals(request, response.getRequest());
        
        Assert.assertEquals(10, response.toJSON().length());
    }

}
