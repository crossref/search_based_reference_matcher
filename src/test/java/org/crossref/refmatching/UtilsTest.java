package org.crossref.refmatching;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import org.crossref.common.utils.ResourceUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;

/**
 *
 * @author Dominika Tkaczyk
 */
public class UtilsTest {

    @Test
    public void testNormalize() {
        Assert.assertEquals("bath", Utils.normalize("Båth"));
        Assert.assertEquals("155-9", Utils.normalize("155–9"));
        Assert.assertEquals("9* 1 million", Utils.normalize("9· 1 million"));
        Assert.assertEquals(
                "psychologische lang unveroffentlichter philipps-universitat.",
                Utils.normalize("Psychologische Läng Unveröffentlichter "
                        + "Philipps-Universität."));
        Assert.assertEquals("\"integral invariants of ito' s equations,\"",
                Utils.normalize("“Integral invariants of Itô’ s equations,”"));
        Assert.assertEquals(
                "m. v. alfimov, \"photonics of supramolecular nanostructures,\"",
                Utils.normalize("M. V. Alfimov, “Photonics of Supramolecular "
                        + "Nanostructures,”"));
        Assert.assertEquals("guezennec", Utils.normalize("Guézennec"));
        Assert.assertEquals("5'-amp-activated",
                Utils.normalize("5′-AMP-activated"));
        Assert.assertEquals("kuca", Utils.normalize("Kuča"));
        Assert.assertEquals("scharf", Utils.normalize("Schärf"));
        Assert.assertEquals("simon", Utils.normalize("Simón"));
        Assert.assertEquals("2 years", Utils.normalize("2\u00a0years"));
        Assert.assertEquals("pp 201-224", Utils.normalize("pp 201–224"));
        Assert.assertEquals("physiological b cell death",
                Utils.normalize("Physiological β cell death"));
        Assert.assertEquals("pflugers arch", Utils.normalize("Pflügers Arch"));
        Assert.assertEquals("norskov", Utils.normalize("Nørskov"));
        Assert.assertEquals("goncalves", Utils.normalize("Gonçalves"));
        Assert.assertEquals("ruszczynski", Utils.normalize("Ruszczyński"));
        Assert.assertEquals("ronnqvist", Utils.normalize("Rönnqvist"));
        Assert.assertEquals(
                "uberblick. in m. prinz (hg.), der lange weg in den uberfluss: "
                + "anfange (s. 437 - 461). paderborn et al.: schoningh",
                Utils.normalize("Überblick. In M. Prinz (Hg.), Der lange Weg in "
                        + "den Überfluss: Anfänge (S. 437 – 461). Paderborn et "
                        + "al.: Schöningh"));
        Assert.assertEquals("leseche", Utils.normalize("Lesèche"));
        Assert.assertEquals("fur padagogische",
                Utils.normalize("für Pädagogische"));
        Assert.assertEquals("baroja-fernandez e, munoz",
                Utils.normalize("Baroja-Fernández E, Muñoz"));
        Assert.assertEquals("dell'acqua tipicita",
                Utils.normalize("Dell’Acqua tipicità"));
    }

    @Test
    public void testStringSimilarity() {
        Assert.assertEquals(1., Utils.stringSimilarity("rest Dire Straits",
                                                "Dire Straits", false, true),
                     0.01);
        Assert.assertEquals(0.78, Utils.stringSimilarity("rDire Straits est",
                                                  "Straits Dire",
                                                   false, true),
                     0.01);
        Assert.assertEquals(0.69, Utils.stringSimilarity("ree Dire Straits",
                                                  "Dear Stratssts", false, true),
                     0.01);
        Assert.assertEquals(0.2, Utils.stringSimilarity("Dire Straits", 
                                                 "Slayerwfqw", false, true),
                     0.01);

        Assert.assertEquals(1., Utils.stringSimilarity("apiDirę Stráits",
                                                "DIre Straîtś", true, true),
                     0.01);
        Assert.assertEquals(0.83, Utils.stringSimilarity("Dire StráîtS rest",
                                                  "Straits Dirę", true, true),
                     0.01);
        Assert.assertEquals(0.75, Utils.stringSimilarity("DIRe Sträitś",
                                                  "rest DęaR Stráts rest", true,
                                                  true),
                     0.01);
        Assert.assertEquals(0.45, Utils.stringSimilarity("Dire Straitśno",
                                                  "rest Sláyer", true, true),
                     0.01);
        
        Assert.assertEquals(1., Utils.stringSimilarity("Dire Straits",
                                            "Dire Straits", false, false),
                     0.01);
        Assert.assertEquals(0.58, Utils.stringSimilarity("Dire Straits",
                                            "Straits Dire", false, false),
                     0.01);
        Assert.assertEquals(0.78, Utils.stringSimilarity("Dire Straits",
                                            "Dear Strats", false, false),
                     0.01);
        Assert.assertEquals(0.22, Utils.stringSimilarity("Dire Straits", "Slayer",
                                                  false, false),
                     0.01);

        Assert.assertEquals(1., Utils.stringSimilarity("Dirę Stráits",
                                                "DIre Straîtś", true, false),
                     0.01);
        Assert.assertEquals(0.58, Utils.stringSimilarity("Dire StráîtS",
                                                "Straits Dirę", true, false),
                     0.01);
        Assert.assertEquals(0.78, Utils.stringSimilarity("DIRe Sträitś",
                                                    "DęaR Stráts", true, false),
                     0.01);
        Assert.assertEquals(0.22, Utils.stringSimilarity("Dire Straitś", "Sláyer",
                                                    true, false),
                     0.01);
    }

    @Test
    public void testCompleteLastPage() {
        Assert.assertEquals("1-8", Utils.completeLastPage("1-8"));
        Assert.assertEquals("1-89", Utils.completeLastPage("1-89"));
        Assert.assertEquals("10-18", Utils.completeLastPage("10-8"));
        Assert.assertEquals("1009-8", Utils.completeLastPage("1009-8"));
        Assert.assertEquals("1325-1328", Utils.completeLastPage("1325-8"));
        Assert.assertEquals("1325-1328", Utils.completeLastPage("1325-28"));
        Assert.assertEquals("1325-128", Utils.completeLastPage("1325-128"));
        Assert.assertEquals("1356-1708", Utils.completeLastPage("1356-708"));
        Assert.assertEquals("26090-26189", Utils.completeLastPage("26090-189"));
        Assert.assertEquals("26090-26099", Utils.completeLastPage("26090-9"));
        Assert.assertEquals("745511554741389877-0340022019", Utils.completeLastPage("745511554741389877-0340022019"));

    }
    
    @Test
    public void testStdHeaders() {
        String file = UtilsTest.class.getResource("/auth/auth.json").getPath();
        
        Map<String, String> headers = Utils.createStdHeaders(file);
        Assert.assertEquals(3, headers.size());
        Assert.assertEquals("Bearer auth-key", headers.get("Authorization"));
        Assert.assertEquals("user-agent; mailto:matcher@matcher.org",
                headers.get("User-Agent"));
        Assert.assertEquals("matcher@matcher.org", headers.get("Mailto"));
    }
    
    @Test
    public void testParseReferencesSingle() throws IOException {
        List<ReferenceData> parsed = Utils.parseInputReferences(InputType.STRING,
                "input ref", "\n");
        Assert.assertEquals(1, parsed.size());
        Assert.assertEquals(ReferenceType.UNSTRUCTURED,
                parsed.get(0).getReference().getType());
        Assert.assertEquals("input ref",
                parsed.get(0).getReference().getFormattedString());
        
        parsed = Utils.parseInputReferences(InputType.STRING,
                "{\"volume\": \"37\", \"year\": \"1997\", \"author\": \"Chan\"}",
                "\n");
        Assert.assertEquals(1, parsed.size());
        Assert.assertEquals(ReferenceType.STRUCTURED,
                parsed.get(0).getReference().getType());
        Assert.assertEquals("Chan",
                parsed.get(0).getReference().getFieldValue("author"));
        Assert.assertEquals("37",
                parsed.get(0).getReference().getFieldValue("volume"));
        Assert.assertEquals("1997",
                parsed.get(0).getReference().getFieldValue("year"));
    }
    
    @Test
    public void testParseReferencesMultiple() throws IOException {
        String fn = "/test-inputs/sample-ref-strings-2000.txt";
        List<String> strings = ResourceUtils.readResourceAsLines(fn);
        List<ReferenceData> parsed = Utils.parseInputReferences(InputType.FILE,
                UtilsTest.class.getResource(fn).getPath(), "\n");
        
        Assert.assertEquals(2000, parsed.size());
        for (int i = 0; i < strings.size(); i++) {
            Assert.assertEquals(ReferenceType.UNSTRUCTURED,
                    parsed.get(i).getReference().getType());
            Assert.assertEquals(strings.get(i),
                    parsed.get(i).getReference().getFormattedString());
        }
        
        parsed = Utils.parseInputReferences(InputType.STRING,
                "{\"volume\": \"37\", \"year\": \"1997\", \"author\": "
                + "\"Chan\"}\nref str 128\n{\"first-page\": \"56\", "
                + "\"volume-title\": \"Leviathan\"}", "\n");
        
        Assert.assertEquals(3, parsed.size());
        
        Assert.assertEquals(ReferenceType.STRUCTURED,
                parsed.get(0).getReference().getType());
        Assert.assertEquals("Chan",
                parsed.get(0).getReference().getFieldValue("author"));
        Assert.assertEquals("37",
                parsed.get(0).getReference().getFieldValue("volume"));
        Assert.assertEquals("1997",
                parsed.get(0).getReference().getFieldValue("year"));
        
        Assert.assertEquals(ReferenceType.UNSTRUCTURED,
                parsed.get(1).getReference().getType());
        Assert.assertEquals("ref str 128",
                parsed.get(1).getReference().getFormattedString());
        
        Assert.assertEquals(ReferenceType.STRUCTURED,
                parsed.get(2).getReference().getType());
        Assert.assertEquals("56",
                parsed.get(2).getReference().getFieldValue("first-page"));
        Assert.assertEquals("Leviathan",
                parsed.get(2).getReference().getFieldValue("volume-title"));
    }
    
    @Test
    public void testParseReferencesArray() throws IOException {
        String fn = "/test-inputs/sample-refs-3000.json";
        JSONArray refs = new JSONArray(ResourceUtils.readResourceAsString(fn));
        List<ReferenceData> parsed = Utils.parseInputReferences(InputType.FILE,
                UtilsTest.class.getResource(fn).getPath(), "unused");

        Assert.assertEquals(3000, parsed.size());
        for (int i = 0; i < refs.length(); i++) {
            Object o = refs.get(i);
            if (o instanceof String) {
                Assert.assertEquals(ReferenceType.UNSTRUCTURED,
                        parsed.get(i).getReference().getType());
                Assert.assertEquals((String) o,
                        parsed.get(i).getReference().getFormattedString());
            } else {
                Assert.assertEquals(ReferenceType.STRUCTURED,
                        parsed.get(i).getReference().getType());
                Assert.assertTrue(((JSONObject) o).similar(
                        parsed.get(i).getReference().getMetadataAsJSON()));
            }
        }
        
        parsed = Utils.parseInputReferences(InputType.STRING,
                "[{\"volume\": \"37\", \"year\": \"1997\", \"author\": "
                + "\"Chan\"}, \"ref str 128\", {\"first-page\": \"56\", "
                + "\"volume-title\": \"Leviathan\"}]", "\n");
        
        Assert.assertEquals(3, parsed.size());
        
        Assert.assertEquals(ReferenceType.STRUCTURED,
                parsed.get(0).getReference().getType());
        Assert.assertEquals("Chan",
                parsed.get(0).getReference().getFieldValue("author"));
        Assert.assertEquals("37",
                parsed.get(0).getReference().getFieldValue("volume"));
        Assert.assertEquals("1997",
                parsed.get(0).getReference().getFieldValue("year"));
        
        Assert.assertEquals(ReferenceType.UNSTRUCTURED,
                parsed.get(1).getReference().getType());
        Assert.assertEquals("ref str 128",
                parsed.get(1).getReference().getFormattedString());
        
        Assert.assertEquals(ReferenceType.STRUCTURED,
                parsed.get(2).getReference().getType());
        Assert.assertEquals("56",
                parsed.get(2).getReference().getFieldValue("first-page"));
        Assert.assertEquals("Leviathan",
                parsed.get(2).getReference().getFieldValue("volume-title"));
    }
    
}