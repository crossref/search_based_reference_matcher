package org.crossref.refmatching;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;
import org.crossref.common.utils.ResourceUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;

/**
 *
 * @author Dominika Tkaczyk
 */
public class CandidateSelectorTest {    
    
    private static final String TEST_FILE =
            "/api-responses/unstructured-ref-response-1.json";
    
    CandidateSelector selector = new CandidateSelector(null);
    
    @Test
    public void testFilterCandidates() {
        String content = ResourceUtils.readResourceAsString(TEST_FILE);
        JSONArray json = new JSONObject(content).getJSONObject("message")
                .optJSONArray("items");
        List<Candidate> candidates = 
                StreamSupport.stream(json.spliterator(), false).map(
                    item -> new Candidate((JSONObject) item))
                .collect(Collectors.toList());
        
        List<Candidate> filtered =
                selector.filterCandidates("query", new ArrayList<>(), 0);
        Assert.assertTrue(filtered.isEmpty());
        
        filtered = selector.filterCandidates("query", candidates, 50);
        Assert.assertEquals(1, filtered.size());
        Assert.assertEquals(candidates.get(0), filtered.get(0));
        
        filtered = selector.filterCandidates("query", candidates, 30);
        Assert.assertEquals(1, filtered.size());
        Assert.assertEquals(candidates.get(0), filtered.get(0));
        
        filtered = selector.filterCandidates("query", candidates, 10);
        Assert.assertEquals(4, filtered.size());
        Assert.assertEquals(candidates.get(0), filtered.get(0));
        Assert.assertEquals(candidates.get(1), filtered.get(1));
        Assert.assertEquals(candidates.get(2), filtered.get(2));
        Assert.assertEquals(candidates.get(3), filtered.get(3));
    }
    
    @Test
    public void testGetSearchCandidateQuery() {
        String refString =
            "[1]D. Tkaczyk, P. Szostek, M. Fedoryszak, P. J. Dendek, and Ł. "
            + "Bolikowski, “CERMINE: automatic extraction of structured "
            + "metadata from scientific literature,” International Journal "
            + "on Document Analysis and Recognition (IJDAR), vol. 18, no. 4, "
            + "pp. 317–335, 2015.";
        Assert.assertEquals(refString, selector.getSearchCandidateQuery(
                new Reference(refString)));

        Map<String, String> fields = new HashMap<>();
        fields.put("author", "Tkaczyk");
        fields.put("volume", "18");
        fields.put("first-page", "317");
        fields.put("year", "2015");
        fields.put("journal-title", "IJDAR");
        Reference reference = new Reference(fields);
        Assert.assertEquals("Tkaczyk IJDAR 2015 18 317",
                selector.getSearchCandidateQuery(reference));
    }
    
}