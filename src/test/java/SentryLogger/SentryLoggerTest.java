package SentryLogger;

import java.io.IOException;
import org.crossref.refmatching.SentryLogger;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;


public class SentryLoggerTest {

    private static final String TEST_MESSAGE = "TEST";
    private static final String EXCEPTION_MSG = "Testing Sentry exception";
    private final SentryLogger sentryLogger = SentryLogger.getLoggerClass(SentryLoggerTest.class);
    private static Throwable ioExeception;
    private static final String DSN = "";

    @Before
    public void initTest() {
        ioExeception = new IOException(EXCEPTION_MSG);
        sentryLogger.setDSN(DSN);
    }

    @Test
    public void exceptionEventTest() {
        // for manual testing with a proper dsn
        if (!DSN.isEmpty()) {
        sentryLogger.setCaptureEvents(true);
        Assert.assertTrue("If capture is enabled, return should be true (submitted to Sentry)",
                sentryLogger.recordException(ioExeception, TEST_MESSAGE));
        }
    }

    @Test
    public void exceptionNotCaptured() {
        sentryLogger.setCaptureEvents(false);
        Assert.assertFalse("If capture is not enabled, return should be false",
                sentryLogger.recordException(ioExeception, TEST_MESSAGE));
    }
}
